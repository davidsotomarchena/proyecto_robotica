#!/bin/bash

# sudo chmod 666 /dev/ttyACM0

# arduino-cli lib install LedControl
# arduino-cli core install arduino:avr

arduino-cli compile -b arduino:avr:uno $1
arduino-cli upload -b arduino:avr:uno $1 --port /dev/ttyACM0
