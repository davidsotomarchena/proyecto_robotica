const { contextBridge } = require("electron");
const { exec } = require("child_process");

function deal_output(error, stdout, stderr) {
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    }
    if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
}

const command = "electron/send_ardu electron/TresMatrices/";

var last_url = "/";

contextBridge.exposeInMainWorld("myAPI", {
    prueba: ()=>{
        exec("ls -la",deal_output);
    },
    altavoz: ()=>{
        exec(command + "Estatico_Altavoz",deal_output);
    },
    neutro: ()=>{
        exec(command + "Estatico_Neutra",deal_output);
    },
    corazon: ()=>{
        exec(command + "Guino_Corazon",deal_output);
    },
    tick: ()=>{
        exec(command + "Guino_Tick",deal_output);
    },
    triste: ()=>{
        exec(command + "Rapido_Triste",deal_output);
    },
    interr: ()=>{
        exec(command + "Estatico_Interrogacion",deal_output);
    },
    apagar: ()=>{
        exec(command + "Apager",deal_output);
    },
    set_url: (url)=>{
        last_url = url;
    },
    get_url: ()=>{
        return last_url;
    }
});
