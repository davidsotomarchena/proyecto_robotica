const m = require("mithril");
const BottomButton = require("./Layouts/BottomButton");

const menus = [
    require("./Index"),
    require("./Sound/SoundMenu"),
    require("./Word/WordMenu"),
    require("./Success")
];

const games = [
    require("./Sound/Identification"),
    require("./Sound/Identification_2"),
    require("./Word/Different"),
    require("./Word/Repetition"),
    require("./Word/Identification"),
];

let routes = {};

for (let each of menus){
    routes = {
        ...routes,
        [each.url]: {
            render: ()=>{
                return m(BottomButton, m(each));
            }
        }
    };
}

for (let each of games){
    routes = {
        ...routes,
        [each.url]: {
            render: ()=>{
                return m(BottomButton, m(each.comp));
            }
        }
    };
}

m.route(document.body, "/main_menu",{
    ...routes,
    "/undone":{
        render:function(){
            return m(BottomButton, m("h1","UNFINISHED"));
        }
    }
});
