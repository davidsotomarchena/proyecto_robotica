const m = require("mithril");
const Success = require("../Success");

const url = "/word/different";

function comp () {
    window.myAPI.set_url(url);
    let list = [
        [["cama", true], ["cocina", false], ["galleta", false]],
        [["mamá", false], ["papá", false], ["mío", true]],
        [["perro", false], ["gato", false], ["no", true]],
        [["vaca", false], ["pollo", false], ["ropa", true]]
    ];
    let idx = Math.floor(Math.random()*list.length);
    let item = list[idx];
    let show_rand = [];
    for (let i = 0; i < 3; i++){
        let j = Math.floor(Math.random()*item.length);
        show_rand.push(item[j]);
        item.splice(j,1);
    }
    let img = [];
    for (let each of show_rand){
        if (each[1] == true){
            img.push(
                m(
                    m.route.Link,
                    {
                        selector: "div.text_game",
                        href: Success.url
                    },
                    m(".text_in", each[0])
                )
            );
        } else {
            img.push(
                m(
                    ".text_game",
                    {
                        onclick: function(){
                            let aud = document.getElementsByTagName("audio")[0];
                            aud.play();
                        }
                    },
                    m(".text_in",each[0])
                )
            );
        }
    }
    window.myAPI.interr();
    return {view:()=>[
        m(
            "audio.controls",
            {
                autoplay: false,
                controls: false
            },
            [
                m("source", {
                    src:"media/"+ "fail.mp3",
                    type:"audio/ogg"
                })
                , "no ogg"
            ]
        ),
        img
    ]};
}

module.exports = {
    url: url,
    comp: comp
};


