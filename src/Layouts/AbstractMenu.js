const m = require("mithril");

module.exports = {
    create: function(sel, arr){
        let res = [];
        for(let each of arr){
            res.push(m(
                m.route.Link,
                {
                    selector: "div." + sel,
                    href: each[0]
                },
                m(".text_in", each[1])
            ));
        }
        return () => {return res;};
    }
};
