const { app, BrowserWindow } = require("electron");
const path = require("path");

const createWindow = () => {
    const win = new BrowserWindow({
        frame: false,
        fullscreen: true,
        webPreferences: {
            preload: path.join(__dirname, "load_apis.js")
        }
    });
    win.loadFile("../gui/index.html");
};

app.whenReady().then(() => {
    createWindow();
});
