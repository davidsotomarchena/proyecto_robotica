# Robo-psicología

Este repositorio tiene el código necesario para la aplicación de la raspi

## electron
- main: Crea la aplicación que se

- load_apis: Crea el objeto windows, el cual nos permite acceder desde la página
web a los programas que interactúan directamente con el SO.

- load_ardu: archivo sh que pone todo en marcha para actuar con el arduino

### Tres Matrices

Archivos de arduino, suceptibles de cambio.

## gui
aplicación web que se podría correr en un explorador normal salvo por la api que
nos facilita electron.

- app : Generado externamente por webpack
- style : Generado externamente por sass
- Media/* : Archivos estáticos que se usan internamente.
- index: Template básico de html

## sass
Archivos de sass que se compilan para dar el estilo a toda la página

- colors: Archivo con todos los colores en variables

- base: Archivo principal

## src

Javascript que hacen el "cerebro" de la aplicación.

# External credits

- Sound effects from Minecraft
- Sound effects from https://quicksounds.com
