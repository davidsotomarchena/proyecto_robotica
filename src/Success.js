const m = require("mithril");

var i = 0;

module.exports = {
    url: "/success",
    view:() => {
        if (i % 2 === 0) {
            window.myAPI.corazon();
        } else {
            window.myAPI.tick();
        }
        i++;
        return [
            m(
                "audio.controls",
                {
                    autoplay: true,
                    volume: 0.5
                },
                [
                    m("source", {
                        src:"media/"+"win.mp3",
                        type:"audio/mpeg"
                    })
                    , "no ogg"
                ]
            ),
            m(
                m.route.Link,
                {
                    selector: "div",
                    href: window.myAPI.get_url()
                },
                m("h1.success", "¡Muy Bien!")

            )
        ];
    }
};
