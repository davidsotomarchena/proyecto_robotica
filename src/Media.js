const media = {
    pig: {
        string: "cerdo",
        image: "pig.webp",
        sound: "pig.ogg",
        word: "cerdo_w.ogg",
    },
    chicken: {
        string: "pollo",
        image: "chicken.webp",
        sound: "chicken.ogg",
        word: "pollo_w.ogg",
    },
    cow: {
        string: "vaca",
        image: "cow.webp",
        sound: "cow.ogg",
        word: "vaca_w.ogg",
    },
    dog: {
        string: "perro",
        image: "dog.webp",
        sound: "dog.ogg",
        word: "perro_w.ogg",
    },
    house: {
        string: "casa",
        word: "casa_w.ogg",
    },
    cookie: {
        string: "galleta",
        word: "galleta_w.ogg",
    },
    cat: {
        string: "gato",
        word: "gato_w.ogg",
    },
    mother: {
        string: "mamá",
        word: "mama_w.ogg",
    },
    more: {
        string: "más",
        word: "mas_w.ogg",
    },
    father: {
        string: "papá",
        word: "papa_w.ogg",
    },
    mine: {
        string: "mío",
        word: "mio_w.ogg",
    },
    no: {
        string: "no",
        word: "no_w.ogg",
    },
    yes: {
        string: "sí",
        word: "si_w.ogg",
    },
};

module.exports = {
    media: media,
    pathify: (item) => {
        return "media/"+ item;
    }
};
