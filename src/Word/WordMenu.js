const {create} = require("../Layouts/AbstractMenu");
const Identification = require("./Identification");
const Different = require("./Different");
const Repetition = require("./Repetition");

const ret = create(
    "word_menu",
    [
        [Repetition.url, "Repite"],
        [Identification.url, "Identifica"],
        [Different.url, "Diferente"]
    ]
);

module.exports = {
    url: "/word",
    view: ret
};
