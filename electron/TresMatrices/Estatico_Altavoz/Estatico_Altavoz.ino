#include "LedControl.h" 

LedControl lc=LedControl(7,5,6,3); // Los numeros se refieren a que pin de ARDUINO tienes en cada uno de los terminales

byte Ojos_abiertos[] = 
 
{B00111100,
 
B01000010,
 
B10000001,
 
B10011001,
 
B10011001,
 
B10000001,
 
B01000010,
 
B00111100};

byte Ojos_cerrados1[] =

{B00011000,

B00100100,

B01000010,

B01011010,

B01011010,

B01000010,

B00100100,

B00011000};

byte Ojos_cerrados2[] =

{B00011000,

B00011000,

B00100100,

B00111100,

B00111100,

B00100100,

B00011000,

B00011000};

byte Ojos_cerrados3[] =

{B00011000,

B00011000,

B00011000,

B00011000,

B00011000,

B00011000,

B00011000,

B00011000};

byte Altavoz0[] = {
 
B00000000,
 
B00000000,
 
B00000000,
 
B00000000,
 
B01111110,

B00111100,
 
B00011000,
 
B00011000};

byte Altavoz1[] = {
 
B00000000,
 
B00000000,
 
B00011000,
 
B00000000,
 
B01111110,

B00111100,
 
B00011000,
 
B00011000};

byte Altavoz2[] = {
 
B00111100,
 
B00000000,
 
B00011000,
 
B00000000,
 
B01111110,

B00111100,
 
B00011000,
 
B00011000};


void setup() {
  // put your setup code here, to run once:
  // El numero que colocamos como argumento de la funcion se refiere a la direccion del decodificador
 
  lc.shutdown(0,false);
  lc.setIntensity(0,5);// La valores estan entre 1 y 15  
  lc.clearDisplay(0);
  
  lc.shutdown(1,false);
  lc.setIntensity(1,5);// La valores estan entre 1 y 15  
  lc.clearDisplay(1);

  lc.shutdown(2,false);
  lc.setIntensity(2,5);// La valores estan entre 1 y 15  
  lc.clearDisplay(2);
}

void loop() {
  // put your main code here, to run repeatedly:
  Representar(Ojos_abiertos,0);
  Representar2(Altavoz0,500);
  Representar2(Altavoz1,500);
  Representar2(Altavoz2,500);
  Representar(Ojos_cerrados1,50);
  Representar(Ojos_cerrados2,50);
  Representar(Ojos_cerrados3,50);
  Representar(Ojos_cerrados2,50);
  Representar(Ojos_cerrados1,50);
  

}


void Representar(byte *Datos,int retardo) //Funcion para la representacion de bytes de datos para una matriz de 8x8 
{
  for (int i = 0; i < 8; i++)  
  {
    lc.setColumn(0,i,Datos[7-i]);
    lc.setColumn(1,i,Datos[7-i]);
  }
  delay(retardo);
}

void Representar2(byte *Datos,int retardo) //Funcion para la representacion de bytes de datos para una matriz de 8x8 
{
  for (int i = 0; i < 8; i++)  
  {
    lc.setColumn(2,i,Datos[7-i]);
  }
  delay(retardo);
}
