const path = require('path');

module.exports = {
  entry: './src/Router.js',
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, 'gui'),
  },
  mode: 'production'
};
