const m = require("mithril");
const Success = require("../Success");
const Media = require("../Media");


const url = "/word/ident";

function comp () {
    window.myAPI.set_url(url);
    let list = Object.keys(Media.media);
    let idx = Math.floor(Math.random()*list.length);
    let item = list[idx];
    let show = [item];
    list.splice(idx,1);
    for (let i = 0; i <  2; i++){
        let j = Math.floor(Math.random()*list.length);
        show.push(list[j]);
        list.splice(j,1);
    }
    let show_rand = [];
    for (let i = 0; i <  3; i++){
        let j = Math.floor(Math.random()*show.length);
        show_rand.push(show[j]);
        show.splice(j,1);
    }
    let img = [];
    for (let each of show_rand){
        if (item == each){
            img.push(
                m(
                    m.route.Link,
                    {
                        selector: "div.text_game",
                        href: Success.url
                    },
                    m(".text_in", Media.media[each].string)
                )
            );
        } else {
            img.push(
                m(
                    ".text_game",
                    {
                        onclick: function(){
                            let aud = document.getElementsByTagName("audio")[0];
                            aud.play();
                        }
                    },
                    m(".text_in",Media.media[each].string)
                )
            );
        }
    }
    window.myAPI.interr();
    return {view:()=>[
        m(
            "audio.controls",
            {
                autoplay: true,
                controls: true
            },
            [
                m("source", {
                    src:Media.pathify(Media.media[item].word),
                    type:"audio/ogg"
                })
                , "no ogg"
            ]
        ),
        img
    ]};
}

module.exports = {
    url: url,
    comp: comp
};

