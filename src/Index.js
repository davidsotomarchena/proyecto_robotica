const {create} = require("./Layouts/AbstractMenu");
const WordMenu = require("./Word/WordMenu");
const SoundMenu = require("./Sound/SoundMenu");

const ret = create(
    "main_menu",
    [
        [SoundMenu.url, "Sonidos"],
        [WordMenu.url, "Palabras"],
    ]
);

module.exports = {
    url: "/main_menu",
    view: ()=>{window.myAPI.neutro(); return ret();}
};
