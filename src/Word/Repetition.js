const m = require("mithril");
const Success = require("../Success");
const Media = require("../Media");

const url = "/word/repeat";

function comp () {
    window.myAPI.set_url(url);
    let list = Object.keys(Media.media);
    let idx = Math.floor(Math.random()*list.length);
    let item = list[idx];
    window.myAPI.interr();
    return {view:()=>[
        m(
            "audio.controls",
            {
                autoplay: true,
                controls: true
            },
            [
                m("source", {
                    src:Media.pathify(Media.media[item].word),
                    type:"audio/ogg"
                })
                , "no ogg"
            ]
        ),
        m(
            m.route.Link,
            {
                selector: "div.center_button",
                href: Success.url
            },
            m(".text_in", Media.media[item].string)
        )
    ]};
}

module.exports = {
    url: url,
    comp: comp
};

