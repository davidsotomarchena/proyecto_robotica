const {create} = require("../Layouts/AbstractMenu");
const Ident = require("./Identification");
const Ident_2 = require("./Identification_2");

const ret = create(
    "sound_menu",
    [
        [Ident.url, "Identifica"],
        [Ident_2.url, "Identifica 2"],
    ]
);

module.exports = {
    url: "/sound",
    view: ret
};
