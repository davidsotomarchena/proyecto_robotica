const m = require("mithril");

module.exports = {
    view: function(vnode) {
        return m("main.Bottom_Button",[
            m("div", vnode.children),
            m("nav.bottom_menu", [
                m(m.route.Link, {href: "/main_menu"},
                    m("img",{
                        height: "50px",
                        src: "media/home_icon.png"
                    })
                )
            ])
        ]);
    }
};
